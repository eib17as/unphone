unPhone expander casing: 3D print models
===

When using an expander board attached to the back of the unPhone, it makes
sense to 3D print some enclosure parts to keep the electronics safe.

Here are some 3D models for various enclosure sections that fit te expander
and the main case:

[ ![](UnphoneCaseParts-250x108.png "unPhone casing 3D models") ](UnphoneCaseParts.png)

There's a base, middle and top, with the latter two available in both solid
format and with push-out holes for cables access or featherwing access.
Multiple middle sections can be fitted to cater for assemblies of various
sizes.

All the parts clip together, with the base part held in place by the expander
board fixings.

The models files (STL) are here:

- [the base](BaseFinal.stl)
- [the middle section](MidFinal.stl)
- [a perforated middle section](MidSection_Perforated.stl)
- [the top](TopFinal.stl)
- [the top with push-out featherwing-sized access holes](Top_Perforated.stl)

(Thanks to Peter Hemmins for authoring these!)
