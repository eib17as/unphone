// unphone.cpp
// core library

#include "unphone.h"

bool UNPHONE_DBG = false; // debug switch

// the LCD and touch screen //////////////////////////////////////////////////
Adafruit_HX8357 *unPhone::tftp;
Adafruit_STMPE610 *unPhone::tsp;
void unPhone::backlight(bool on) { // turn the backlight on or off
  if(on) digitalWrite(IOExpander::BACKLIGHT, HIGH);
  else   digitalWrite(IOExpander::BACKLIGHT, LOW);
}

// the accelerometer //////////////////////////////////////////////////////
Adafruit_LSM303_Accel_Unified *unPhone::accelp;

// initialise unPhone hardware
void unPhone::begin() {
  Serial.begin(115200);  // init the serial line

  // fire up I²C, and the unPhone's IOExpander library
  recoverI2C();
  Wire.begin();
  Wire.setClock(400000); // rates > 100k used to trigger an IOExpander bug...?
  IOExpander::begin();

  // instantiate the display...
  tftp = new Adafruit_HX8357( // spin5 moves LCD_DC to 21
    IOExpander::LCD_CS, LCD_DC, IOExpander::LCD_RESET
  );
  tsp = new Adafruit_STMPE610(IOExpander::TOUCH_CS);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT, LOW);
  tftp->begin(HX8357D);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT, HIGH);
  tftp->setTextWrap(false);
  // TODO clear screen to start with: tftp->fillScreen(HX8357_BLACK);

  // ...and the touch screen
  bool status = tsp->begin();
  if(!status) {
    E("failed to start touchscreen controller\n");
  } else {
    D("worked\n");
  }

  // init the SD card
  status = SD.begin(IOExpander::SD_CS);
  if(!status) {
    E("SD.begin failed\n");
  } else {
    D("SD.begin OK\n");
  }

  // init touch screen GPIOs (used for vibe motor)
  tsp->writeRegister8(0x17, 12); // use GPIO 2 & 3 (bcd)
  tsp->writeRegister8(0x13, 12); // all gpios as out, minimise power on unused

  // initialise the buttons
  pinMode(IOExpander::BUTTON2, INPUT);
  pinMode(BUTTON1, INPUT_PULLUP);
  pinMode(BUTTON3, INPUT_PULLUP);
  Serial.println("pinmodes done");
  Serial.printf("board spin = %u\n", IOExpander::getVersionNumber());

  // battery voltage sensing    TODO is setting resolution here correct?
  pinMode(VBAT_SENSE, INPUT);
  analogReadResolution(12); // 10 bit = 0-1023, 11 = 0-2047, 12 = 0-4095
  analogSetPinAttenuation(
    VBAT_SENSE,
    ADC_6db // 0db is 0-1V, 2.5db is 0-1.5V, 6db is 0-2.2v, 11db is 0-3.3v
  );

  // the accelerometer (with a "unique" ID)
  accelp = new Adafruit_LSM303_Accel_Unified(54321);
  if(!accelp->begin()) // problem detecting the sensor?
    Serial.println("oops, no LSM303 detected ... check your wiring?!");

  // set up IR_LED pin
  pinMode(IR_LEDS, OUTPUT);
} // begin()

void unPhone::vibe(bool on) {
  if(on)
    tsp->writeRegister8(0x10, 8);   // set GPIO3 HIGH to start vibe
  else
    tsp->writeRegister8(0x11, 8);   // set GPIO3 LOW to stop vibe
}

void unPhone::rgb(uint8_t red, uint8_t green, uint8_t blue) {
  digitalWrite(IOExpander::LED_RED, red);
  digitalWrite(IOExpander::LED_GREEN, green);
  digitalWrite(IOExpander::LED_BLUE, blue);
}

bool unPhone::button1() { return digitalRead(unPhone::BUTTON1) == LOW; }
bool unPhone::button2() { return digitalRead(unPhone::BUTTON2) == LOW; }
bool unPhone::button3() { return digitalRead(unPhone::BUTTON3) == LOW; }

// try to recover I2C bus in case it's locked up...
// NOTE: only do this in setup **BEFORE** Wire.begin!
void unPhone::recoverI2C() {
  pinMode(SCL, OUTPUT);
  pinMode(SDA, OUTPUT);
  digitalWrite(SDA, HIGH);

  for(int i = 0; i < 10; i++) { // 9th cycle acts as NACK
    digitalWrite(SCL, HIGH);
    delayMicroseconds(5);
    digitalWrite(SCL, LOW);
    delayMicroseconds(5);
  }

  // a STOP signal (SDA from low to high while SCL is high)
  digitalWrite(SDA, LOW);
  delayMicroseconds(5);
  digitalWrite(SCL, HIGH);
  delayMicroseconds(2);
  digitalWrite(SDA, HIGH);
  delayMicroseconds(2);

  // I2C bus should be free now... a short delay to help things settle
  delay(200);
}

// power management chip API /////////////////////////////////////////////////
const byte unPhone::BM_I2Cadd   = 0x6b; // the chip lives here on I²C
const byte unPhone::BM_Watchdog = 0x05; // charge termination/timer cntrl reg
const byte unPhone::BM_OpCon    = 0x07; // misc operation control register
const byte unPhone::BM_Status   = 0x08; // system status register
const byte unPhone::BM_Version  = 0x0a; // vender / part / revision status reg

float unPhone::batteryVoltage() { // get the battery voltage
  float voltage;
  voltage = analogRead(VBAT_SENSE);
  voltage = (voltage/ 4095) * 4.4;
  return voltage;
}

RTC_DATA_ATTR int bootCount = 0; // how many times we booted (since reset)

void unPhone::printWakeupReason() {
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0:
      Serial.println("Wakeup caused by external signal using RTC_IO");
      break;
    case ESP_SLEEP_WAKEUP_EXT1:
      Serial.println("Wakeup caused by external signal using RTC_CNTL");
      break;
    case ESP_SLEEP_WAKEUP_TIMER:
      Serial.println("Wakeup caused by timer");
      break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD:
      Serial.println("Wakeup caused by touchpad");
      break;
    case ESP_SLEEP_WAKEUP_ULP:
      Serial.println("Wakeup caused by ULP program");
      break;
    default:
      Serial.printf("Wakeup not caused by deep sleep: %d\n",wakeup_reason);
      break;
  }
}

// check for power off states and do BM shipping mode (when on bat) or ESP
// deep sleep (when on USB 5V)
bool unPhone::checkPowerSwitch() {
// TODO this would be better as an interrupt, driven off GPIO_NUM36 (the
// unphone power switch slider)

  // what is the state of the power switch? (non-zero = on, which is
  // physically slid away from the USB socket)
  uint8_t inputPwrSw = IOExpander::digitalRead(IOExpander::POWER_SWITCH);

  bool usbPowerOn = // bit 2 of status register indicates if USB connected
    bitRead(getRegister(BM_I2Cadd, BM_Status), 2);

  if(!inputPwrSw) {  // when power switch off
    if(!usbPowerOn) { // and usb unplugged we go into shipping mode
      // tell BM to stop supplying power (from the battery)
      Serial.println("settign shipping mode true");
      setShipping(true);
    } else { // power switch off and usb plugged in we sleep
      Serial.println("enabling wakeup on 36 and doing shippig and deep_sleep");
      esp_sleep_enable_ext0_wakeup(GPIO_NUM_36, 0); // 1 = High, 0 = Low

      // cludge: LCD (and other peripherals) will still be powered when we're
      // on USB; the next call turns the LCD backlight off, but would be
      // preferable if we could cut the 5V to all but the BM (which needs it
      // for charging)...?
      IOExpander::digitalWrite(IOExpander::BACKLIGHT, LOW);

      // deep sleep, wait for wakeup on GPIO
      esp_deep_sleep_start();
    }
  }

  return usbPowerOn;
}

// ask BM chip to shutdown or start up
void unPhone::setShipping(bool value) {
  byte result;
  if(value) {
    result=getRegister(BM_I2Cadd, BM_Watchdog);  // state of timing register
    bitClear(result, 5);                         // clear bit 5...
    bitClear(result, 4);                         // and bit 4 to disable...
    setRegister(BM_I2Cadd, BM_Watchdog, result); // WDT (REG05[5:4] = 00)

    result=getRegister(BM_I2Cadd, BM_OpCon);     // operational register
    bitSet(result, 5);                           // set bit 5 to disable...
    setRegister(BM_I2Cadd, BM_OpCon, result);    // BATFET (REG07[5] = 1)
  } else {
    result=getRegister(BM_I2Cadd, BM_Watchdog);  // state of timing register
    bitClear(result, 5);                         // clear bit 5...
    bitSet(result, 4);                           // and set bit 4 to enable...
    setRegister(BM_I2Cadd, BM_Watchdog, result); // WDT (REG05[5:4] = 01)

    result=getRegister(BM_I2Cadd, BM_OpCon);     // operational register
    bitClear(result, 5);                         // clear bit 5 to enable...
    setRegister(BM_I2Cadd, BM_OpCon, result);    // BATFET (REG07[5] = 0)
  }
}

// I2C helpers to drive the power management chip
// TODO share with IOExp versions?
void unPhone::setRegister(byte address, byte reg, byte value) {
  write8(address, reg, value);
}
byte unPhone::getRegister(byte address, byte reg) {
  byte result;
  result=read8(address, reg);
  return result;
}
void unPhone::write8(byte address, byte reg, byte value) {
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.write((uint8_t)value);
  Wire.endTransmission();
}
byte unPhone::read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}

// the LoRa board and TTN LoRaWAN ///////////////////////////////////////////

// these callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain)
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

// TODO pass mydata through from in lmic_do_send API
uint8_t mydata[] = "Hello, World!";
int loops=1;
uint8_t loopcount[] = " Loops: ";
osjob_t unPhone::sendjob;

// schedule TX every this many seconds (might become longer due to duty
// cycle limitations)
const unsigned TX_INTERVAL = 20;

// pin mapping
const lmic_pinmap lmic_pins = {
  .nss = IOExpander::LORA_CS,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = IOExpander::LORA_RESET,
  .dio = {39, 34, LMIC_UNUSED_PIN}  // modified to suit connection on unphone
};

void onEvent(ev_t ev) {
  Serial.print(os_getTime());
  Serial.print(": ");
  switch(ev) {
    case EV_SCAN_TIMEOUT:
      Serial.println(F("EV_SCAN_TIMEOUT"));
      break;
    case EV_BEACON_FOUND:
      Serial.println(F("EV_BEACON_FOUND"));
      break;
    case EV_BEACON_MISSED:
      Serial.println(F("EV_BEACON_MISSED"));
      break;
    case EV_BEACON_TRACKED:
      Serial.println(F("EV_BEACON_TRACKED"));
      break;
    case EV_JOINING:
      Serial.println(F("EV_JOINING"));
      break;
    case EV_JOINED:
      Serial.println(F("EV_JOINED"));
      break;
    case EV_RFU1:
      Serial.println(F("EV_RFU1"));
      break;
    case EV_JOIN_FAILED:
      Serial.println(F("EV_JOIN_FAILED"));
      break;
    case EV_REJOIN_FAILED:
      Serial.println(F("EV_REJOIN_FAILED"));
      break;
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      if(LMIC.dataLen) {
        // data received in rx slot after tx
        Serial.print(F("Data Received: "));
        Serial.write(LMIC.frame+LMIC.dataBeg, LMIC.dataLen);
        Serial.println();
      }
      // schedule next transmission
      os_setTimedCallback(
        &unPhone::sendjob,
        os_getTime()+sec2osticks(TX_INTERVAL),
        unPhone::lmic_do_send
      );
      break;
    case EV_LOST_TSYNC:
      Serial.println(F("EV_LOST_TSYNC"));
      break;
    case EV_RESET:
      Serial.println(F("EV_RESET"));
      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
      Serial.println(F("EV_RXCOMPLETE"));
      break;
    case EV_LINK_DEAD:
      Serial.println(F("EV_LINK_DEAD"));
      break;
    case EV_LINK_ALIVE:
      Serial.println(F("EV_LINK_ALIVE"));
      break;
     default:
      Serial.println(F("Unknown event"));
      break;
  }
}

void unPhone::lmic_do_send(osjob_t* j) {
  // check if there is not a current TX/RX job running
  if(LMIC.opmode & OP_TXRXPEND) {
    D("OP_TXRXPEND, not sending");
  } else {
    // prepare upstream data transmission at the next possible time
    LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
    loopcount[0]=+1;
    LMIC_setTxData2(1, loopcount, sizeof(loopcount)-1, 0);
    D("Packet queued");
  }
  // next TX is scheduled after TX_COMPLETE event
}

void unPhone::lmic_init(
  devaddr_t devaddr, xref2u1_t nwkKey, xref2u1_t artKey
) {
  // LMIC init
  Serial.println("doing os_init()...");
  os_init(); // if lora fails then will stop, allowing panic mess to be seen

  // reset the MAC state; session and pending data transfers will be discarded
  Serial.println("doing LMIC_reset()...");
  LMIC_reset();

  // set static session parameters instead of dynamically establishing session
  // by joining the network, precomputed session parameters are provided
  Serial.println("doing LMIC_setSession()...");
  LMIC_setSession (0x1, devaddr, (u1_t *) nwkKey, (u1_t *) artKey);

  // Set up the channels used by the Things Network, which correspond
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set. All g-band except the last
  // (which is g2-band):
  LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
  LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);
  LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
  LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
  LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
  LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
  LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
  LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
  LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);

  // TTN defines an additional channel at 869.525Mhz using SF9 for class B
  // devices' ping slots. LMIC does not have an easy way to define set this
  // frequency and support for class B is spotty and untested, so this
  // frequency is not configured here.
  // Disable link check validation
  LMIC_setLinkCheckMode(0);
  // set data rate and transmit power (note: txpow seems to be ignored by lib)
  LMIC_setDrTxpow(DR_SF7, 14);
}
