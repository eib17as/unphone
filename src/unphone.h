// unphone.h
// core definitions and includes

#ifndef UNPHONE_H
#define UNPHONE_H

#include "unphelf.h"              // shared definitions
#include "IOExpander.h"           // the IO expander chip

// support for unPhone's TCA9555 IO expander chip (also injected into
// libraries that talk to components connected to the expander); behaviour is
// the same as the normal scoped methods except when using pins defined in
// IOExpander.h (which are OR'd with 0x40)
#define digitalWrite IOExpander::digitalWrite // call...
#define digitalRead  IOExpander::digitalRead  // ...ours...
#define pinMode      IOExpander::pinMode      // ...please

#include <SD.h>                   // the SD card
#include <lmic.h>                 // IBM LMIC (LoraMAC-in-C) library
#include <hal/hal.h>              // hardware abstraction for LMIC on Arduino
#include <SPI.h>                  // the SPI bus
#include <Adafruit_GFX.h>         // core graphics library
#include <Adafruit_HX8357.h>      // tft display
#include <Adafruit_STMPE610.h>    // touch screen
#include <Adafruit_ImageReader.h> // image-reading functions
#include <Wire.h>                 // I²C comms on the Arduino
#include <IOExpander.h>           // unPhone's IOExpander (controlled via I²C)
#include <Adafruit_Sensor.h>      // base class etc. for sensor abstraction
#include <Adafruit_LSM303_U.h>    // the accelerometer sensor
#include <driver/i2s.h>           // ESP I²S bus
#include <IRremote.h>             // infra-red remote control (ESP version)

// code specific to unphone hardware
class unPhone {
public:
  static void begin(); // initialise hardware

  // buttons
  static const uint8_t BUTTON1 = 33;                  // left one
  static const uint8_t BUTTON2 = IOExpander::BUTTON2; // middle one
  static const uint8_t BUTTON3 = 34;                  // right one
  static bool button1();                              // register...
  static bool button2();                              // ...button...
  static bool button3();                              // ...presses

  static void vibe(bool);             // vibe motor on or off
  static bool checkPowerSwitch();     // if power switch is off shutdown
  static void printWakeupReason();    // what woke us up?
  static void recoverI2C();           // deal with i2c hangs

  // the display and touch screen
  static Adafruit_HX8357 *tftp;
  static Adafruit_STMPE610 *tsp;
  static const uint8_t LCD_DC =  21;  // from spin 6, previously IOExp 7
  static void backlight(bool);        // turn the backlight on or off

  // calibration data for converting raw touch data to screen coordinates
  static const uint16_t TS_MINX = 3800;
  static const uint16_t TS_MAXX =  100;
  static const uint16_t TS_MINY =  100;
  static const uint16_t TS_MAXY = 3750;

  static const uint8_t IR_LEDS = 12;  // the IR LED pins

  // the accelerometer //////////////////////////////////////////////////////
  static Adafruit_LSM303_Accel_Unified *accelp;

  // the RGB LED
  static void rgb(uint8_t red, uint8_t green, uint8_t blue);

  // power management chip API //////////////////////////////////////////////
  // TODO simplify, add USB_VSENSE
  static const uint8_t VBAT_SENSE = 35; // battery voltage pin
  static float batteryVoltage(); // get the battery voltage
  static const byte BM_I2Cadd;   // the chip lives here on I²C
  static const byte BM_Watchdog; // charge termination/timer control register
  static const byte BM_OpCon;    // misc operation control register
  static const byte BM_Status;   // system status register
  static const byte BM_Version;  // vender / part / revision status register
  static void setShipping(bool value); // tells BM chip to shut down
  static void setRegister(byte address, byte reg, byte value); //
  static byte getRegister(byte address, byte reg);             // I²C...
  static void write8(byte address, byte reg, byte value);      // ...helpers
  static byte read8(byte address, byte reg);                   //

  // the LoRa board and TTN LoRaWAN /////////////////////////////////////////
  static osjob_t sendjob;
  static void lmic_init(devaddr_t, xref2u1_t, xref2u1_t);
  static void lmic_do_send(osjob_t*);

  // TODO merge in the IOExpander here
}; // class unPhone

// macros for debug (and error) calls to Serial.printf, with (D) and without
// (DD) new line, and to Serial.println (DDD) TODO combine with next
#ifdef UNPHONE_PRODUCTION_BUILD
# define D(args...)
# define E(args...)
#else
extern bool UNPHONE_DBG;     // debug switch
# define D_ON  UNPHONE_DBG = 1;
# define DD(args...)  if(UNPHONE_DBG) Serial.printf(args);
# define D_OFF UNPHONE_DBG = 0;
# define D(args...) if(UNPHONE_DBG) printf(args);
# define E(args...) printf(args);
#endif

// debugging infrastructure; setting different DBGs true triggers prints ////
#define dbg(b, s)       if(b) Serial.print(s)
#define dbf(b, ...)     if(b) Serial.printf(__VA_ARGS__)
#define dln(b, s)       if(b) Serial.println(s)
#define startupDBG      true
#define loopDBG         true
#define monitorDBG      true
#define netDBG          true
#define miscDBG         true
#define analogDBG       true
#define otaDBG          true
#define touchDBG        false
static const char *TAG = "MAIN";        // ESP logger debug tag

// delay/yield/timing and time-slicing macros
#define WAIT_A_SEC   vTaskDelay(    1000/portTICK_PERIOD_MS); // 1 second
#define WAIT_SECS(n) vTaskDelay((n*1000)/portTICK_PERIOD_MS); // n seconds
#define WAIT_MS(n)   vTaskDelay(       n/portTICK_PERIOD_MS); // n millis
extern int loopIter; // main task loop iteration counter

#endif
