// Spin5SlowDraw.ino
// self-contained example sketch illustrating slow text drawing on the LCD

#include <SPI.h>                // the SPI bus
#include <Wire.h>               // I²C comms on the Arduino
#include "Adafruit_GFX.h"       // core graphics library
#include "Adafruit_HX8357.h"    // tft display local hacked version
#include "IOExpander.h"         // unPhone's IOExpander (controlled via I²C)
#include "Adafruit_Sensor.h"    // sensors abstraction
#include "Adafruit_LSM303_U.h"  // accelerometer library

#define UNPHONE_SPIN 5
// #define UNPHONE_SPIN 4

#if UNPHONE_SPIN > 4
Adafruit_HX8357 tft =
  Adafruit_HX8357(IOExpander::LCD_CS, IOExpander::LCD_DC, IOExpander::LCD_RESET);
#else
Adafruit_HX8357 tft =
  Adafruit_HX8357(IOExpander::LCD_CS, 33, IOExpander::LCD_RESET);
#endif

Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);

void setup() {
  // TODO uncomment the following and it speeds up... why?!
  // accel.begin();
  // because it seems that setClock can't be called before begin; accel.begin
  // includes Wire.begin()....
  // so if begin is first, then setClock successfully speeds up the bus
  // now to see if that has messed up the IOExpander as previously
  Wire.begin();
  Wire.setClock(400000);

  IOExpander::begin();
  tft.begin(HX8357D);
  tft.fillScreen(HX8357_BLACK);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH);

  tft.setTextSize(2);
  tft.setCursor(0,300);
  tft.println(" THIS TEXT MESSAGE SHOULD");
  tft.println("     APPEAR QUICKLY");
  tft.println("   NOT SCROLL INTO VIEW...");
  
  delay(2000);
  tft.setCursor(0,360);
  tft.println(" (...random 2nd message");
  tft.println("    to show redraw)");
}

void loop() {
}
