# "main" pseudo-component makefile

UNPHONE_INC_DIR=../../../../src
UNPHONE_PARENT_DIR=../../../../..
UNPHONE_SRC_DIR=../../../src
UNPHONE_LIB_DIR=../../../lib

CPPFLAGS += -I$(UNPHONE_INC_DIR) -I$(UNPHONE_PARENT_DIR) \
  -Wno-all \
  -Wno-error=return-type -Wno-write-strings -Wno-conversion-null \
  -Wno-return-type -Wno-pointer-arith -Wno-cpp -Wno-unused-variable \

OTATHING_LIBS := $(UNPHONE_SRC_DIR) \
  $(UNPHONE_LIB_DIR)/ESPAsyncWebServer/src \
  $(UNPHONE_LIB_DIR)/AsyncTCP/src/ \
  $(UNPHONE_LIB_DIR)/OneWire \
  $(UNPHONE_LIB_DIR)/ArduinoJson/src \
  $(UNPHONE_LIB_DIR)/WiFiManager \

COMPONENT_SRCDIRS += $(OTATHING_LIBS)
COMPONENT_ADD_INCLUDEDIRS := $(OTATHING_LIBS)
